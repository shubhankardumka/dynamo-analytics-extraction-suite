from facebook_business import FacebookAdsApi
from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.adsinsights import AdsInsights
from facebook_business.adobjects.adreportrun import AdReportRun
from facebook_business.adobjects.adset import AdSet
from facebook_business.adobjects.ad import Ad
from facebook_business.session import FacebookSession
from facebook_business import FacebookAdsApi
from facebook_business.adobjects.helpers import *
from facebook_business.adobjects import *
from facebook_business.adobjects.campaign import Campaign
from bson import json_util
from google.cloud import bigquery
from google.cloud.exceptions import NotFound
import os
import json
import time
import codecs


######################################Device Platform Breakdown##################################

my_app_id = '137901210196131'
my_app_secret = 'c39eb9e2461ca78d4ce48b5e5907542d'
my_access_token = 'EAAB9a6Cl2KMBAO3KozNiSfvUmotSDWPbCpEm8E5J1m7ZAmYND2FpxMGjqMpu5fLklecenwuPBF3i2DBvE8yqagg1d8C6SryY8nyUHAO0PjXd01pBlea6msOu3GLDuSVVdoCnZB1E5BNcydImbjhMpY2an29mCvuc0o66jRcgZDZD'

FacebookAdsApi.init(my_app_id,my_app_secret,my_access_token)


accounts = AdAccount('act_1611792828939424')

params = {
    'date_preset': 'lifetime', 
		'level': 'campaign',
		
		'time_increment': 1,
		'use_account_attribution_setting':'true',
		'breakdowns': ['device_platform','publisher_platform'],
        'fields': [
			AdsInsights.Field.date_start,
			AdsInsights.Field.date_stop,
			AdsInsights.Field.account_name,
			AdsInsights.Field.campaign_name,
			AdsInsights.Field.objective,
			AdsInsights.Field.actions,
			AdsInsights.Field.spend,
			AdsInsights.Field.impressions,
			AdsInsights.Field.clicks,
			AdsInsights.Field.reach,
			AdsInsights.Field.video_10_sec_watched_actions,
			AdsInsights.Field.video_30_sec_watched_actions,
			AdsInsights.Field.video_avg_time_watched_actions,
			AdsInsights.Field.video_p100_watched_actions,
			AdsInsights.Field.video_p25_watched_actions,
			AdsInsights.Field.video_p50_watched_actions,
			AdsInsights.Field.video_p75_watched_actions,
			AdsInsights.Field.website_purchase_roas
			
		]
    }
	
async_job = accounts.get_insights(params=params, is_async='true')
status = async_job.remote_read()
while status['async_percent_completion'] < 100:
	time.sleep(1)	
	status = async_job.remote_read()

	
data = async_job.get_result(params={'limit': '5000'})
json_docs = []


for AdsInsights in data:
	x=AdsInsights.export_all_data()
	json_doc = json.dumps(x, default=json_util.default)
	json_docs.append(json_doc)
	

with open('Fb_Device_Publisher.json', 'w') as outfile:
	result=str(json_docs)
	result_fin2=result.replace("['",'')
	result_fin3=result_fin2.replace("']",'')
	result_fin4=result_fin3.replace("',",'\n')
	result_fin5=result_fin4.replace(" ",'')
	result_fin6=result_fin5.replace("'",'')
	codecs.encode(result_fin6, encoding='utf-8', errors='strict')
	outfile.write(result_fin6)

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Integration-3805ef657d39.json"
client = bigquery.Client()

dataset_ref = client.dataset('Account_ASUAGFE')
table_ref = dataset_ref.table('Fb_Device_Platform')

try:
	table = client.get_table(table_ref)
        
	if table:
		client.delete_table(table_ref)
		
except NotFound as error:
	a=1
	
	
job_config = bigquery.LoadJobConfig()
job_config.autodetect = True
job_config.source_format = 'NEWLINE_DELIMITED_JSON'

with open('Fb_Device_Publisher.json', 'rb') as source_file:
    job = client.load_table_from_file(
        source_file, table_ref, job_config=job_config)  # Start the job.



assert job.job_type == 'load'
job.result()  

assert job.state == 'DONE'

########################################### Age Gender Breakdowns###########################################

params = {
    'date_preset': 'lifetime', 
		'level': 'campaign',
		
		'time_increment': 1,
		'use_account_attribution_setting':'true',
		'breakdowns': ['age','gender'],
        'fields': [
			AdsInsights.Field.date_start,
			AdsInsights.Field.date_stop,
			AdsInsights.Field.account_name,
			AdsInsights.Field.campaign_name,
			AdsInsights.Field.objective,
			AdsInsights.Field.actions,
			AdsInsights.Field.spend,
			AdsInsights.Field.impressions,
			AdsInsights.Field.clicks,
			AdsInsights.Field.reach,
			AdsInsights.Field.video_10_sec_watched_actions,
			AdsInsights.Field.video_30_sec_watched_actions,
			AdsInsights.Field.video_avg_time_watched_actions,
			AdsInsights.Field.video_p100_watched_actions,
			AdsInsights.Field.video_p25_watched_actions,
			AdsInsights.Field.video_p50_watched_actions,
			AdsInsights.Field.video_p75_watched_actions,
			AdsInsights.Field.website_purchase_roas
			
		]
    }
	
async_job = accounts.get_insights(params=params, is_async='true')
status = async_job.remote_read()
while status['async_percent_completion'] < 100:
	time.sleep(1)	
	status = async_job.remote_read()

	
data = async_job.get_result(params={'limit': '2000'})
json_docs = []


for AdsInsights in data:
	x=AdsInsights.export_all_data()
	json_doc = json.dumps(x, default=json_util.default)
	json_docs.append(json_doc)
	

with open('Fb_Age_Gender.json', 'w') as outfile:
	result=str(json_docs)
	result_fin2=result.replace("['",'')
	result_fin3=result_fin2.replace("']",'')
	result_fin4=result_fin3.replace("',",'\n')
	result_fin5=result_fin4.replace(" ",'')
	result_fin6=result_fin5.replace("'",'')
	codecs.encode(result_fin6, encoding='utf-8', errors='strict')
	outfile.write(result_fin6)

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Integration-3805ef657d39.json"
client = bigquery.Client()

dataset_ref = client.dataset('Account_ASUAGFE')
table_ref = dataset_ref.table('Fb_Age_Gender')

try:
	table = client.get_table(table_ref)
        
	if table:
		client.delete_table(table_ref)
		
except NotFound as error:
	a=1
	
	
job_config = bigquery.LoadJobConfig()
job_config.autodetect = True
job_config.source_format = 'NEWLINE_DELIMITED_JSON'

with open('Fb_Age_Gender.json', 'rb') as source_file:
    job = client.load_table_from_file(
        source_file, table_ref, job_config=job_config)  # Start the job.



assert job.job_type == 'load'
job.result()  

assert job.state == 'DONE'

########################################### Country Breakdowns###########################################

params = {
    'date_preset': 'lifetime', 
		'level': 'campaign',
		
		'time_increment': 1,
		'use_account_attribution_setting':'true',
		'breakdowns': ['country'],
        'fields': [
			AdsInsights.Field.date_start,
			AdsInsights.Field.date_stop,
			AdsInsights.Field.account_name,
			AdsInsights.Field.campaign_name,
			AdsInsights.Field.objective,
			AdsInsights.Field.actions,
			AdsInsights.Field.spend,
			AdsInsights.Field.impressions,
			AdsInsights.Field.clicks,
			AdsInsights.Field.reach,
			AdsInsights.Field.video_10_sec_watched_actions,
			AdsInsights.Field.video_30_sec_watched_actions,
			AdsInsights.Field.video_avg_time_watched_actions,
			AdsInsights.Field.video_p100_watched_actions,
			AdsInsights.Field.video_p25_watched_actions,
			AdsInsights.Field.video_p50_watched_actions,
			AdsInsights.Field.video_p75_watched_actions,
			AdsInsights.Field.website_purchase_roas
			
		]
    }
	
async_job = accounts.get_insights(params=params, is_async='true')
status = async_job.remote_read()
while status['async_percent_completion'] < 100:
	time.sleep(1)	
	status = async_job.remote_read()

	
data = async_job.get_result(params={'limit': '10000'})
json_docs = []


for AdsInsights in data:
	x=AdsInsights.export_all_data()
	json_doc = json.dumps(x, default=json_util.default)
	json_docs.append(json_doc)
	

with open('Fb_Country.json', 'w') as outfile:
	result=str(json_docs)
	result_fin2=result.replace("['",'')
	result_fin3=result_fin2.replace("']",'')
	result_fin4=result_fin3.replace("',",'\n')
	result_fin5=result_fin4.replace(" ",'')
	result_fin6=result_fin5.replace("'",'')
	codecs.encode(result_fin6, encoding='utf-8', errors='strict')
	outfile.write(result_fin6)

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Integration-3805ef657d39.json"
client = bigquery.Client()

dataset_ref = client.dataset('Account_ASUAGFE')
table_ref = dataset_ref.table('Fb_Country')

try:
	table = client.get_table(table_ref)
        
	if table:
		client.delete_table(table_ref)
		
except NotFound as error:
	a=1
	
	
job_config = bigquery.LoadJobConfig()
job_config.autodetect = True
job_config.source_format = 'NEWLINE_DELIMITED_JSON'

with open('Fb_Country.json', 'rb') as source_file:
    job = client.load_table_from_file(
        source_file, table_ref, job_config=job_config)  # Start the job.



assert job.job_type == 'load'
job.result()  

assert job.state == 'DONE'

##