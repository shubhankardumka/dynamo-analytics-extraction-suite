//**********************************************************Facebook*******************************************************

////////////////////////////////////////////1 Device Platform Actions////////////////////////////////////////////////////////

function FB_ASU_Device_Platform_Actions() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "FB_Device_Platform_Actions_Clean"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "select dp.date_start, dp.date_stop, dp.account_name, dp.campaign_name, dp.objective,dp.device_platform,dp.publisher_platform,dp.Spend, dp.impressions, dp.clicks , dp.reach,de.Likes, de.Page_engagement,de.Post_Engagement, de.Post_Reaction, de.Photo_View, de.Post, de.Link_Click, de.Comment, de.Offsite_Conversion_Fb_Pixel_Lead, de.Landing_Page_View, de.Offsite_Conversion, de.Video_View from ( select date_start,date_stop,account_name, campaign_name, objective, device_platform,publisher_platform, sum(Spend) Spend, sum(Impressions) Impressions, sum(Clicks) Clicks, sum(Reach) Reach from Account_ASUAGFE.Fb_Device_Platform group by date_start,date_stop,account_name, campaign_name, objective, device_platform,publisher_platform) dp left join (SELECT date_start,date_stop,account_name,campaign_name,objective,device_platform,publisher_platform, SUM(Likes) Likes,SUM(Page_Engagement) Page_Engagement,SUM(Post_Engagement) Post_Engagement,SUM(Post_Reaction) Post_Reaction,SUM(Photo_View) Photo_View,SUM(Post) Post,SUM(Link_Click) Link_Click,SUM(Comment) Comment,SUM(Offsite_Conversion_Fb_Pixel_Lead) Offsite_Conversion_Fb_Pixel_Lead,SUM(Landing_Page_View) Landing_Page_View,SUM(Offsite_Conversion) Offsite_Conversion,SUM(Video_View) Video_View  from ( select date_start,date_stop,account_name,campaign_name,objective,device_platform,publisher_platform,case when(action_type)='like' then Value else 0 end Likes,case when(action_type)='page_engagement' then Value else 0 end Page_Engagement,case when(action_type)='post_engagement' then Value else 0 end Post_Engagement,case when(action_type)='post_reaction' then Value else 0  end Post_Reaction,case when(action_type)='photo_view' then Value else 0 end Photo_View ,case when(action_type)='post' then Value else 0 end Post,case when(action_type)='link_click' then value else 0 end Link_Click,case when(action_type)='comment' then value else 0 end Comment,case when(action_type)='offsite_conversion.fb_pixel_lead' then Value else 0 end Offsite_Conversion_Fb_Pixel_Lead,case when(action_type)='landing_page_view' then Value else 0 end Landing_Page_View,case when(action_type)='offsite_conversion' then Value else 0 end Offsite_Conversion,case when(action_type)='video_view' then Value else 0 end Video_View from (select date_start,date_stop,account_name, campaign_name,objective,device_platform,publisher_platform,h.value,h.action_type from Account_ASUAGFE.Fb_Device_Platform, unnest(actions) as h)dt ) de group by date_start,date_stop,account_name,campaign_name,objective,device_platform,publisher_platform order by date_start )de on dp.date_start=de.date_start and dp.date_stop=de.date_stop and dp.account_name=de.account_name and dp.campaign_name=de.campaign_name and dp.objective=de.objective and dp.device_platform=de.device_platform and dp.publisher_platform=de.publisher_platform order by date_start"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}


////////////////////////////////////////////2 Device Platform Video Views////////////////////////////////////////////////////////

function FB_ASU_Device_Platform_Video_Views() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "FB_Device_Platform_Video_Views_Clean"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "select date_start,date_stop,account_name, campaign_name,objective,device_platform,publisher_platform,case when a.value is null then 0 else a.value end video_10_sec_watched_actions,case when b.value is null then 0 else b.value end video_30_sec_watched_actions,case when c.value is null then 0 else c.value end video_avg_time_watched_actions,case when d.value is null then 0 else d.value end  video_p100_watched_actions,case when e.value is null then 0 else e.value end video_p25_watched_actions,case when f.value is null then 0 else f.value end  video_p50_watched_actions,case when g.value is null then 0 else g.value end video_p75_watched_actions from Account_ASUAGFE.Fb_Device_Platform , unnest(video_10_sec_watched_actions) as a,unnest(video_30_sec_watched_actions) as b,unnest(video_avg_time_watched_actions) as c,unnest(video_p100_watched_actions) as d,unnest(video_p25_watched_actions) as e,unnest(video_p50_watched_actions) as f,unnest(video_p75_watched_actions) as g"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

////////////////////////////////////////////3 Age Gender Actions////////////////////////////////////////////////////////

function FB_ASU_Age_Gender_Actions() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "FB_Age_Gender_Actions_Clean"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "select dp.date_start, dp.date_stop, dp.account_name, dp.campaign_name, dp.objective,dp.age,dp.gender,dp.Spend, dp.impressions, dp.clicks , dp.reach,de.Likes, de.Page_engagement,de.Post_Engagement, de.Post_Reaction, de.Photo_View, de.Post, de.Link_Click, de.Comment, de.Offsite_Conversion_Fb_Pixel_Lead, de.Landing_Page_View, de.Offsite_Conversion, de.Video_View from ( select date_start,date_stop,account_name, campaign_name,objective, age,gender, sum(Spend) Spend, sum(Impressions) Impressions, sum(Clicks) Clicks, sum(Reach) Reach from Account_ASUAGFE.Fb_Age_Gender group by date_start,date_stop,account_name, campaign_name, objective, age,gender) dp left join ( SELECT date_start,date_stop,account_name,campaign_name,objective,age,gender,SUM(Likes) Likes,SUM(Page_Engagement) Page_Engagement,SUM(Post_Engagement) Post_Engagement,SUM(Post_Reaction) Post_Reaction,SUM(Photo_View) Photo_View,SUM(Post) Post,SUM(Link_Click) Link_Click,SUM(Comment) Comment,SUM(Offsite_Conversion_Fb_Pixel_Lead) Offsite_Conversion_Fb_Pixel_Lead,SUM(Landing_Page_View) Landing_Page_View,SUM(Offsite_Conversion) Offsite_Conversion,SUM(Video_View) Video_View  from (select date_start,date_stop,account_name,campaign_name,objective,age,gender,case when(action_type)='like' then Value else 0 end Likes,case when(action_type)='page_engagement' then Value else 0 end Page_Engagement,case when(action_type)='post_engagement' then Value else 0 end Post_Engagement,case when(action_type)='post_reaction' then Value else 0  end Post_Reaction,case when(action_type)='photo_view' then Value else 0 end Photo_View ,case when(action_type)='post' then Value else 0 end Post,case when(action_type)='link_click' then value else 0 end Link_Click,case when(action_type)='comment' then value else 0 end Comment,case when(action_type)='offsite_conversion.fb_pixel_lead' then Value else 0 end Offsite_Conversion_Fb_Pixel_Lead,case when(action_type)='landing_page_view' then Value else 0 end Landing_Page_View,case when(action_type)='offsite_conversion' then Value else 0 end Offsite_Conversion,case when(action_type)='video_view' then Value else 0 end Video_View from ( select date_start,date_stop,account_name, campaign_name,objective,age,gender,h.value,h.action_type from Account_ASUAGFE.Fb_Age_Gender, unnest(actions) as h) dt ) dg group by date_start,date_stop,account_name,campaign_name,objective,age,gender order by date_start )de on dp.date_start=de.date_start and dp.date_stop=de.date_stop and dp.account_name=de.account_name and dp.campaign_name=de.campaign_name and dp.objective=de.objective and dp.age=de.age and dp.gender=de.gender order by date_start"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

////////////////////////////////////////////4 Country Actions////////////////////////////////////////////////////////

function FB_ASU_Country_Actions() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "FB_Country_Actions_Clean"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "select dp.date_start, dp.date_stop, dp.account_name, dp.campaign_name, dp.objective,dp.country,dp.Spend, dp.impressions, dp.clicks , dp.reach,de.Likes, de.Page_engagement,de.Post_Engagement, de.Post_Reaction, de.Photo_View, de.Post, de.Link_Click, de.Comment, de.Offsite_Conversion_Fb_Pixel_Lead, de.Landing_Page_View, de.Offsite_Conversion, de.Video_View from ( select date_start,date_stop,account_name, campaign_name, objective, country, sum(Spend) Spend, sum(Impressions) Impressions, sum(Clicks) Clicks, sum(Reach) Reach from Account_ASUAGFE.Fb_Country group by date_start,date_stop,account_name, campaign_name, objective, country) dp left join ( SELECT date_start,date_stop,account_name,campaign_name,objective,country, SUM(Likes) Likes,SUM(Page_Engagement) Page_Engagement,SUM(Post_Engagement) Post_Engagement,SUM(Post_Reaction) Post_Reaction,SUM(Photo_View) Photo_View,SUM(Post) Post,SUM(Link_Click) Link_Click,SUM(Comment) Comment,SUM(Offsite_Conversion_Fb_Pixel_Lead) Offsite_Conversion_Fb_Pixel_Lead,SUM(Landing_Page_View) Landing_Page_View,SUM(Offsite_Conversion) Offsite_Conversion,SUM(Video_View) Video_View  from ( select date_start,date_stop,account_name,campaign_name,objective,country,case when(action_type)='like' then Value else 0 end Likes,case when(action_type)='page_engagement' then Value else 0 end Page_Engagement,case when(action_type)='post_engagement' then Value else 0 end Post_Engagement,case when(action_type)='post_reaction' then Value else 0  end Post_Reaction,case when(action_type)='photo_view' then Value else 0 end Photo_View ,case when(action_type)='post' then Value else 0 end Post,case when(action_type)='link_click' then value else 0 end Link_Click,case when(action_type)='comment' then value else 0 end Comment,case when(action_type)='offsite_conversion.fb_pixel_lead' then Value else 0 end Offsite_Conversion_Fb_Pixel_Lead,case when(action_type)='landing_page_view' then Value else 0 end Landing_Page_View,case when(action_type)='offsite_conversion' then Value else 0 end Offsite_Conversion,case when(action_type)='video_view' then Value else 0 end Video_View from ( select date_start,date_stop,account_name, campaign_name,objective,country,h.value,h.action_type from Account_ASUAGFE.Fb_Country, unnest(actions) as h )dt )dg group by Date_start,date_stop,account_name,campaign_name,objective,country order by date_start )de on dp.date_start=de.date_start and dp.date_stop=de.date_stop and dp.account_name=de.account_name and dp.campaign_name=de.campaign_name and dp.objective=de.objective and dp.country=de.country order by date_start"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

//////////////////////////////////////////// Scheduling Scripts////////////////////////////////////////////////////////

//////////////////////////////////////////// 5 Device Platform Actions////////////////////////////////////////////////////////


function FB_ASU_Device_Platform_Actions_Clean_FINAL() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "FB_Device_Platform_Actions_Clean_FINAL"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "SELECT distinct 'CPC_CPE_CPL_CPV' Unit1,'100002' Account_Id, '200004' Campaign_Id, case when extract(MONTH from date_start)=02 then spend*1.430579 when extract(MONTH from date_start)=03 then spend*1.430579 when date_start='2018-03-29' then spend*1.515 else spend end spend1,* FROM  Account_ASUAGFE.FB_Device_Platform_Actions_Clean"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

////////////////////////////////////////////6 Device Platform Video Views////////////////////////////////////////////////////////

function FB_ASU_Device_Platform_Video_Views_Clean_Final() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "FB_Device_Platform_Video_Views_Clean_Final"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "SELECT distinct 'CPC_CPE_CPL_CPV' Unit1,'100002' Account_Id, '200004' Campaign_Id, * FROM  Account_ASUAGFE.FB_Device_Platform_Video_Views_Clean"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

////////////////////////////////////////////7 Age Gender Actions////////////////////////////////////////////////////////

function FB_ASU_Age_Gender_Actions_Clean_FINAL() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "FB_Age_Gender_Actions_Clean_FINAL"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "SELECT distinct 'CPC_CPE_CPL_CPV' Unit1,'100002' Account_Id, '200004' Campaign_Id,case when extract(MONTH from date_start)=02 then spend*1.430579 when extract(MONTH from date_start)=03 then spend*1.430579 when date_start='2018-03-29' then spend*1.515 else spend end spend1, * FROM  Account_ASUAGFE.FB_Age_Gender_Actions_Clean"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

////////////////////////////////////////////8 Country Actions////////////////////////////////////////////////////////

function FB_ASU_Country_Actions_Clean_FINAL() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "FB_Country_Actions_Clean_FINAL"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "SELECT distinct 'CPC_CPE_CPL_CPV' Unit1,'100002' Account_Id, '200004' Campaign_Id,case when extract(MONTH from date_start)=02 then spend*1.430579 when extract(MONTH from date_start)=03 then spend*1.430579 when date_start='2018-03-29' then spend*1.515 else spend end spend1,* FROM  Account_ASUAGFE.FB_Country_Actions_Clean"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

//**********************************************************Adwords*******************************************************

////////////////////////////////////////////9 Adw_Ad_Performance////////////////////////////////////////////////////////

function Adw_ASU_Ad_Performance_Clean_FINAL() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "Adw_Ad_Performance_Clean_FINAL"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "SELECT distinct 'CPC_CPE_CPL_CPV' Unit1,'100002' Account_Id, '200004' Campaign_Id,case when extract (month from Day)=02 then cost*1.430579 when extract (month from Day)=03 then cost*1.430579 when day='2018-03-29' then cost*1.515 else cost end Cost1, * FROM  Account_ASUAGFE.Adw_Ad_Performance"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

////////////////////////////////////////////10 Adw_Agerange_Performace////////////////////////////////////////////////////////

function Adw_ASU_Agerange_Performace_Clean_FINAL() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "Adw_Agerange_Performace_Clean_FINAL"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "SELECT distinct 'CPC_CPE_CPL_CPV' Unit1,'100002' Account_Id, '200004' Campaign_Id,case when extract (month from Day)=02 then cost*1.430579 when extract (month from Day)=03 then cost*1.430579 when day='2018-03-29' then cost*1.515 else cost end Cost1, * FROM  Account_ASUAGFE.Adw_Agerange_Performace"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

////////////////////////////////////////////11 Adw_Campaign_Performance_Main////////////////////////////////////////////////////////

function Adw_ASU_Campaign_Performance_Main_Clean_FINAL() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "Adw_Campaign_Performance_Main_Clean_FINAL"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "SELECT distinct 'CPC_CPE_CPL_CPV' Unit1,'100002' Account_Id, '200004' Campaign_Id,case when extract (month from Day)=02 then cost*1.430579 when extract (month from Day)=03 then cost*1.430579 when day='2018-03-29' then cost*1.515 else cost end Cost1, * FROM  Account_ASUAGFE.Adw_Campaign_Performance_Main"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

////////////////////////////////////////////12 Adw_Gender_Performance////////////////////////////////////////////////////////

function Adw_ASU_Gender_Performance_Clean_FINAL() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "Adw_Gender_Performance_Clean_FINAL"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "SELECT distinct 'CPC_CPE_CPL_CPV' Unit1,'100002' Account_Id, '200004' Campaign_Id,case when extract (month from Day)=02 then cost*1.430579 when extract (month from Day)=03 then cost*1.430579 when day='2018-03-29' then cost*1.515 else cost end Cost1, * FROM  Account_ASUAGFE.Adw_Gender_Performance"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

////////////////////////////////////////////13 Adw_Campaign_Performance_Reach////////////////////////////////////////////////////////

function Adw_ASU_Campaign_Performance_Reach_Clean_FINAL() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "Adw_Campaign_Performance_Reach_Clean_FINAL"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "SELECT distinct 'CPC_CPE_CPL_CPV' Unit1,'100002' Account_Id, '200004' Campaign_Id,case when extract (month from Day)=02 then cost*1.430579 when extract (month from Day)=03 then cost*1.430579 when day='2018-03-29' then cost*1.515 else cost end Cost1, * FROM  Account_ASUAGFE.Adw_Campaign_Performance_Reach"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

////////////////////////////////////////////14 Adw_Geo_Performance////////////////////////////////////////////////////////

function Adw_ASU_Geo_Performance_Clean_FINAL() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "Adw_Geo_Performance_Clean_FINAL"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "SELECT distinct 'CPC_CPE_CPL_CPV' Unit1,'100002' Account_Id, '200004' Campaign_Id,case when extract (month from Day)=02 then cost*1.430579 when extract (month from Day)=03 then cost*1.430579 when day='2018-03-29' then cost*1.515 else cost end Cost1, * FROM  Account_ASUAGFE.Adw_Geo_Performance"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

////////////////////////////////////////////15 Adw_Keywords_Performance////////////////////////////////////////////////////////

function Adw_ASU_Keywords_Performance_Clean_FINAL() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "Adw_Keywords_Performance_Clean_FINAL"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "SELECT distinct 'CPC_CPE_CPL_CPV' Unit1,'100002' Account_Id, '200004' Campaign_Id,case when extract (month from Day)=02 then cost*1.430579 when extract (month from Day)=03 then cost*1.430579 when day='2018-03-29' then cost*1.515 else cost end Cost1, * FROM  Account_ASUAGFE.Adw_Keywords_Performance"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}

////////////////////////////////////////////16 Adw_Video_Performance////////////////////////////////////////////////////////

function Adw_ASU_Video_Performance_Clean_FINAL() {
  var configuration = {
    "query": {
    "useQueryCache": false,
    "destinationTable": {
          "projectId": "integration-187614",
          "datasetId": "Account_ASUAGFE",
          "tableId": "Adw_Video_Performance_Clean_FINAL"
        },
    "writeDisposition": "WRITE_TRUNCATE",
    "createDisposition": "CREATE_IF_NEEDED",
    "allowLargeResults": true,
    "useLegacySql": false,
    "query": "SELECT distinct 'CPC_CPE_CPL_CPV' Unit1,'100002' Account_Id, '200004' Campaign_Id,case when extract (month from Day)=02 then cost*1.430579 when extract (month from Day)=03 then cost*1.430579 when day='2018-03-29' then cost*1.515 else cost end Cost1, * FROM  Account_ASUAGFE.Adw_Video_Performance"
    }
  };
   
  var job = {
    "configuration": configuration
  };
 
  var jobResult = BigQuery.Jobs.insert(job, "integration-187614");
  Logger.log(jobResult);

  // Forces the script to require
https://www.googleapis.com/auth/drive.readonly scope.
DriveApp.getFiles();
}
