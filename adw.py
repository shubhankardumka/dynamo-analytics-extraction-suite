import csv
from googleads import adwords
from pathlib import Path
from google.cloud import bigquery
from google.cloud.exceptions import NotFound

import os
import json
import time
import codecs
import sys 

def main(client):
  # Initialize appropriate service.
	report_downloader = client.GetReportDownloader(version='v201809')

  ############################################### Ad Performance Report ###########################################
  
  # Report Init
	report = {
      'reportName': 'AD_PERFORMANCE_REPORT',
      'dateRangeType': 'ALL_TIME',
      'reportType': 'AD_PERFORMANCE_REPORT',
      'downloadFormat': 'CSV',
      'selector': {
          'fields': ['AccountDescriptiveName', 'CampaignName', 'CreativeDestinationUrl', 'AdType',
                          'AdGroupName', 'CreativeFinalUrls', 'CriterionType', 'DisplayUrl',
                          'Id','Status','Date','Device','DayOfWeek','Headline','AdNetworkType1','AdNetworkType2','CriterionId','CriterionType','AllConversions','Clicks','Conversions','Engagements','Impressions','Interactions','VideoViews','Cost']
      }
  }
	
	# Writing API Results to local file
	with open('Adw_Ad_Performance.csv', 'w') as f:
		report_downloader.DownloadReport(report,f,skip_report_header=True,skip_column_header=False,skip_report_summary=True, include_zero_impressions=False)
	
	# Big Query Init	
	os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Integration-3805ef657d39.json"
	client = bigquery.Client()

	dataset_ref = client.dataset('Account_ASUAGFE')
	table_ref = dataset_ref.table('Adw_Ad_Performance')
	
	try:
		table = client.get_table(table_ref)
        
		if table:
			client.delete_table(table_ref)
		
	except NotFound as error:
		a=1
		
	
	job_config = bigquery.LoadJobConfig()
	job_config.source_format = 'CSV'
	job_config.autodetect = True
	job_config.writeDisposition = 'WRITE_TRUNCATE'

	#BQ Upload
	with open('Adw_Ad_Performance.csv', 'rb') as source_file:
		job = client.load_table_from_file(
			source_file, table_ref, job_config=job_config)  # Start the job.



	assert job.job_type == 'load'
	job.result()  

	assert job.state == 'DONE'
	
	######################################## Age_Range_Performance Report ###########################################
  
  # Report Init
	report1 = {
      'reportName': 'AGE_RANGE_PERFORMANCE_REPORT',
      'dateRangeType': 'ALL_TIME',
      'reportType': 'AGE_RANGE_PERFORMANCE_REPORT',
      'downloadFormat': 'CSV',
      'selector': {
          'fields': ['AccountDescriptiveName', 'CampaignName','Criteria','Id','Status','Date','DayOfWeek','Device','AdNetworkType1','AdNetworkType2','AllConversions','Clicks','Conversions','Engagements','Impressions','Interactions','VideoViews','Cost']
      }
  }
	
	# Writing API Results to local file
	with open('Adw_Agerange_Performance.csv', 'w') as f:
		report_downloader.DownloadReport(report1,f,skip_report_header=True,skip_column_header=False,skip_report_summary=True, include_zero_impressions=True)
	
	# Big Query Init	
	os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Integration-3805ef657d39.json"
	client = bigquery.Client()

	dataset_ref = client.dataset('Account_ASUAGFE')
	table_ref = dataset_ref.table('Adw_Agerange_Performace')
	
	try:
		table = client.get_table(table_ref)
        
		if table:
			client.delete_table(table_ref)
		
	except NotFound as error:
		a=1
		
	job_config = bigquery.LoadJobConfig()
	job_config.source_format = 'CSV'
	job_config.autodetect = True
	job_config.writeDisposition = 'WRITE_TRUNCATE'

	#BQ Upload
	with open('Adw_Agerange_Performance.csv', 'rb') as source_file:
		job = client.load_table_from_file(
			source_file, table_ref, job_config=job_config)  # Start the job.



	assert job.job_type == 'load'
	job.result()  

	assert job.state == 'DONE'
	
	###################################### Campaign_Performance Report##########################################
  
  # Report Init
	report2 = {
      'reportName': 'CAMPAIGN_PERFORMANCE_REPORT',
      'dateRangeType': 'ALL_TIME',
      'reportType': 'CAMPAIGN_PERFORMANCE_REPORT',
      'downloadFormat': 'CSV',
      'selector': {
          'fields': ['AccountTimeZone','AccountDescriptiveName','AdvertisingChannelType','CampaignName','UrlCustomParameters','Device', 'Period','Date','DayOfWeek','AdNetworkType1','AdNetworkType2','AllConversions','Clicks','Conversions','ConversionValue','Engagements','Impressions','Interactions','VideoViews','Cost','ActiveViewImpressions','ActiveViewMeasurableImpressions','ActiveViewMeasurableCost','AllConversionValue','InvalidClicks','ViewThroughConversions']
      }
  }
	
	# Writing API Results to local file
	with open('Adw_Campaign_Performance_1.csv', 'w') as f:
		report_downloader.DownloadReport(report2,f,skip_report_header=True,skip_column_header=False,skip_report_summary=True, include_zero_impressions=True)
	
	# Big Query Init	
	os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Integration-3805ef657d39.json"
	client = bigquery.Client()

	dataset_ref = client.dataset('Account_ASUAGFE')
	table_ref = dataset_ref.table('Adw_Campaign_Performance_Main')
	
	try:
		table = client.get_table(table_ref)
        
		if table:
			client.delete_table(table_ref)
		
	except NotFound as error:
		a=1
		
	job_config = bigquery.LoadJobConfig()
	job_config.source_format = 'CSV'
	job_config.autodetect = True
	job_config.writeDisposition = 'WRITE_TRUNCATE'

	#BQ Upload
	with open('Adw_Campaign_Performance_1.csv', 'rb') as source_file:
		job = client.load_table_from_file(
			source_file, table_ref, job_config=job_config)  # Start the job.



	assert job.job_type == 'load'
	job.result()  

	assert job.state == 'DONE'
	
	# Campaign Performance Frequency Report
	report3 = {
      'reportName': 'CAMPAIGN_PERFORMANCE_REPORT',
      'dateRangeType': 'ALL_TIME',
      'reportType': 'CAMPAIGN_PERFORMANCE_REPORT',
      'downloadFormat': 'CSV',
      'selector': {
          'fields': ['AccountTimeZone','AccountDescriptiveName','AdvertisingChannelType','CampaignName','UrlCustomParameters','Period','Date','AdNetworkType1','AdNetworkType2','AllConversions','Clicks','Conversions','ConversionValue','Engagements','Impressions','ImpressionReach','Interactions','VideoViews','Cost','ActiveViewImpressions','ActiveViewMeasurableImpressions','ActiveViewMeasurableCost','AllConversionValue','InvalidClicks','ViewThroughConversions']
      }
  }
	
	# Writing API Results to local file
	with open('Adw_Campaign_Performance_2.csv', 'w') as f:
		report_downloader.DownloadReport(report3,f,skip_report_header=True,skip_column_header=False,skip_report_summary=True, include_zero_impressions=False)
	
	# Big Query Init	
	os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Integration-3805ef657d39.json"
	client = bigquery.Client()

	dataset_ref = client.dataset('Account_ASUAGFE')
	table_ref = dataset_ref.table('Adw_Campaign_Performance_Reach')
	
	try:
		table = client.get_table(table_ref)
        
		if table:
			client.delete_table(table_ref)
		
	except NotFound as error:
		a=1
		
	job_config = bigquery.LoadJobConfig()
	job_config.source_format = 'CSV'
	job_config.autodetect = True
	job_config.writeDisposition = 'WRITE_TRUNCATE'

	#BQ Upload
	with open('Adw_Campaign_Performance_2.csv', 'rb') as source_file:
		job = client.load_table_from_file(
			source_file, table_ref, job_config=job_config)  # Start the job.



	assert job.job_type == 'load'
	job.result()  

	assert job.state == 'DONE'
	
	############################################ Gender_Performance_Report ###########################################
  
  # Report Init
	report4 = {
      'reportName': 'GENDER_PERFORMANCE_REPORT',
      'dateRangeType': 'ALL_TIME',
      'reportType': 'GENDER_PERFORMANCE_REPORT',
      'downloadFormat': 'CSV',
      'selector': {
          'fields': ['AccountDescriptiveName', 	'CampaignName', 'CampaignStatus', 'Criteria',
                          'CustomerDescriptiveName', 'FinalUrls','UrlCustomParameters',
                          'Date','DayOfWeek','Device','AdNetworkType1','AdNetworkType2','AllConversions','Clicks','Conversions','Engagements','Impressions','Interactions','VideoViews','Cost']
      }
  }
	
	# Writing API Results to local file
	with open('Adw_Gender_Performance.csv', 'w') as f:
		report_downloader.DownloadReport(report4,f,skip_report_header=True,skip_column_header=False,skip_report_summary=True, include_zero_impressions=False)
	
	# Big Query Init	
	os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Integration-3805ef657d39.json"
	client = bigquery.Client()

	dataset_ref = client.dataset('Account_ASUAGFE')
	table_ref = dataset_ref.table('Adw_Gender_Performance')
	
	try:
		table = client.get_table(table_ref)
        
		if table:
			client.delete_table(table_ref)
		
	except NotFound as error:
		a=1
		
	job_config = bigquery.LoadJobConfig()
	job_config.source_format = 'CSV'
	job_config.autodetect = True
	job_config.writeDisposition = 'WRITE_TRUNCATE'

	#BQ Upload
	with open('Adw_Gender_Performance.csv', 'rb') as source_file:
		job = client.load_table_from_file(
			source_file, table_ref, job_config=job_config)  # Start the job.



	assert job.job_type == 'load'
	job.result()  

	assert job.state == 'DONE'
	
	############################################ GEO_Performance_Report ###########################################
  
  # Report Init
	report5 = {
      'reportName': 'GEO_PERFORMANCE_REPORT',
      'dateRangeType': 'ALL_TIME',
      'reportType': 'GEO_PERFORMANCE_REPORT',
      'downloadFormat': 'CSV',
      'selector': {
          'fields': ['AccountDescriptiveName','CampaignName','CustomerDescriptiveName','CityCriteriaId','CountryCriteriaId','Date','DayOfWeek','Device','AdNetworkType1','AdNetworkType2','AllConversions','Clicks','Conversions','Impressions','Interactions','VideoViews','Cost']
      }
  }
	
	# Writing API Results to local file
	with open('Adw_Geo_Performance.csv', 'w') as f:
		report_downloader.DownloadReport(report5,f,skip_report_header=True,skip_column_header=False,skip_report_summary=True, include_zero_impressions=False)
	
	# Big Query Init	
	os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Integration-3805ef657d39.json"
	client = bigquery.Client()

	dataset_ref = client.dataset('Account_ASUAGFE')
	table_ref = dataset_ref.table('Adw_Geo_Performance')
	
	try:
		table = client.get_table(table_ref)
        
		if table:
			client.delete_table(table_ref)
		
	except NotFound as error:
		a=1
		
	job_config = bigquery.LoadJobConfig()
	job_config.source_format = 'CSV'
	job_config.autodetect = True
	job_config.writeDisposition = 'WRITE_TRUNCATE'

	#BQ Upload
	with open('Adw_Geo_Performance.csv', 'rb') as source_file:
		job = client.load_table_from_file(
			source_file, table_ref, job_config=job_config)  # Start the job.



	assert job.job_type == 'load'
	job.result()  

	assert job.state == 'DONE'

	
	
	########################################## Keywords Performance Report ###########################################
  
  # Report Init
	report6 = {
      'reportName': 'KEYWORDS_PERFORMANCE_REPORT',
      'dateRangeType': 'ALL_TIME',
      'reportType': 'KEYWORDS_PERFORMANCE_REPORT',
      'downloadFormat': 'CSV',
      'selector': {
          'fields': ['AccountDescriptiveName', 'CampaignName', 'Criteria','AdGroupName', 'UrlCustomParameters','QualityScore','Date','DayOfWeek','Device','AdNetworkType1','AdNetworkType2','AllConversions','Clicks','Conversions','Engagements','Impressions','Interactions','VideoViews','Cost']
      }
  }
	
	# Writing API Results to local file
	with open('Adw_Keywords_Performance.csv', 'w') as f:
		report_downloader.DownloadReport(report6,f,skip_report_header=True,skip_column_header=False,skip_report_summary=True, include_zero_impressions=False)
	
	# Big Query Init	
	os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Integration-3805ef657d39.json"
	client = bigquery.Client()

	dataset_ref = client.dataset('Account_ASUAGFE')
	table_ref = dataset_ref.table('Adw_Keywords_Performance')
	
	try:
		table = client.get_table(table_ref)
        
		if table:
			client.delete_table(table_ref)
		
	except NotFound as error:
		a=1
	
	job_config = bigquery.LoadJobConfig()
	job_config.source_format = 'CSV'
	job_config.autodetect = True
	job_config.writeDisposition = 'WRITE_TRUNCATE'

	#BQ Upload
	with open('Adw_Keywords_Performance.csv', 'rb') as source_file:
		job = client.load_table_from_file(
			source_file, table_ref, job_config=job_config)  # Start the job.



	assert job.job_type == 'load'
	job.result()  

	assert job.state == 'DONE'
	
	########################################### Video Performance Report ###########################################
  
  # Report Init
	report7 = {
      'reportName': 'VIDEO_PERFORMANCE_REPORT',
      'dateRangeType': 'ALL_TIME',
      'reportType': 'VIDEO_PERFORMANCE_REPORT',
      'downloadFormat': 'CSV',
      'selector': {
          'fields': ['AccountDescriptiveName', 'CampaignName','VideoId', 'VideoTitle','Date','DayOfWeek','AdNetworkType1','AdNetworkType2','AllConversions','Clicks','Conversions','Engagements','Impressions','VideoViews','Cost','VideoQuartile25Rate','VideoQuartile50Rate','VideoQuartile75Rate','VideoQuartile100Rate']
      }
  }
	
	# Writing API Results to local file
	with open('Adw_Video_Performance.csv', 'w') as f:
		report_downloader.DownloadReport(report7,f,skip_report_header=True,skip_column_header=False,skip_report_summary=True, include_zero_impressions=False)
	
	# Big Query Init	
	os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "Integration-3805ef657d39.json"
	client = bigquery.Client()

	dataset_ref = client.dataset('Account_ASUAGFE')
	table_ref = dataset_ref.table('Adw_Video_Performance')
	
	try:
		table = client.get_table(table_ref)
        
		if table:
			client.delete_table(table_ref)
		
	except NotFound as error:
		a=1	
	
	job_config = bigquery.LoadJobConfig()
	job_config.source_format = 'CSV'
	job_config.autodetect = True
	job_config.writeDisposition = 'WRITE_TRUNCATE'

	#BQ Upload
	with open('Adw_Video_Performance.csv', 'rb') as source_file:
		job = client.load_table_from_file(
			source_file, table_ref, job_config=job_config)  # Start the job.



	assert job.job_type == 'load'
	job.result()  

	assert job.state == 'DONE'
	
	
	#main init
if __name__ == '__main__':
  # Initialize client object.
  adwords_client = adwords.AdWordsClient.LoadFromStorage(Path("C:/Users/Admin/Documents/python_projects/googleads.yaml"))

  
  
  main(adwords_client)
  